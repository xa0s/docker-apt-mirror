FROM alpine:3.10
RUN apk --no-cache add perl wget git gzip xz && \
    git clone https://github.com/apt-mirror/apt-mirror.git /tmp && \
    cp /tmp/apt-mirror /usr/bin
COPY ./entrypoint.sh /run
VOLUME ["/aptmirror"]
CMD ["/bin/sh", "/run/entrypoint.sh"]